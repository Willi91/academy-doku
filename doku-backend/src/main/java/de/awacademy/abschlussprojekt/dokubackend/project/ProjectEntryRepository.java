package de.awacademy.abschlussprojekt.dokubackend.project;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectEntryRepository extends JpaRepository<ProjectEntry, Long> {
    List<ProjectEntry> findAllByOrderByCreatedAtDesc();
    List<ProjectEntry> findAllByCategoryEntry_Id(long id);
    List<ProjectEntry> findAllByTechnologyContains(String technology);
}
