package de.awacademy.abschlussprojekt.dokubackend.user;

import javax.validation.constraints.Size;

public class RegistrationDTO {
    @Size(min = 3, max = 50)
    private final String username;

    @Size(min = 5)
    private final String password;

    public RegistrationDTO(String username, String password) {
        this.username = username;
        this.password = password;

    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

}
