package de.awacademy.abschlussprojekt.dokubackend.security;

import de.awacademy.abschlussprojekt.dokubackend.user.User;
import de.awacademy.abschlussprojekt.dokubackend.user.UserRepository;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecurityController {

    private final UserRepository userRepository;


    public SecurityController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/api/sessionUser")
    public User sessionUser(@AuthenticationPrincipal UserDetails userDetails) {
        if (userDetails != null) {
            return userRepository.findByUsername(userDetails.getUsername()).orElse(null);
        }
        return null;

    }


}
