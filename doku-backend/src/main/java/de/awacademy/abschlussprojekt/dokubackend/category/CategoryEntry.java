package de.awacademy.abschlussprojekt.dokubackend.category;

import de.awacademy.abschlussprojekt.dokubackend.project.ProjectEntry;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

@Entity
public class CategoryEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String title;
    private String text;
    private Instant createdAt;

    @OneToMany(mappedBy = "categoryEntry", cascade = CascadeType.ALL)
    private List<ProjectEntry> projectEntries;


    public CategoryEntry() {
    }

    public CategoryEntry(String title, String text) {
        this.title = title;
        this.text = text;
        this.createdAt = Instant.now();
    }

    public String gettitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }


    public List<ProjectEntry> getProjectEntries() {
        return projectEntries;
    }

    public void setProjectEntries(List<ProjectEntry> projectEntries) {
        this.projectEntries = projectEntries;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
