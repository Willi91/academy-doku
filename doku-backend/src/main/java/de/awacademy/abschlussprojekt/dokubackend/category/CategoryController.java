package de.awacademy.abschlussprojekt.dokubackend.category;

import de.awacademy.abschlussprojekt.dokubackend.project.ProjectEntry;
import de.awacademy.abschlussprojekt.dokubackend.project.ProjectEntryDTO;
import de.awacademy.abschlussprojekt.dokubackend.project.ProjectEntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.LinkedList;
import java.util.List;

@RestController
public class CategoryController {

    @Autowired
    private CategoryEntryRepository categoryEntryRepository;
    @Autowired
    private ProjectEntryRepository projectEntryRepository;

    @GetMapping("/api/overview")
    public List<CategoryEntryDTO> showCategories() {
        List<CategoryEntryDTO> response = new LinkedList<>();
        for (CategoryEntry entry : categoryEntryRepository.findAllByOrderByCreatedAtDesc()) {
            response.add(new CategoryEntryDTO(entry.gettitle(), entry.getText(), entry.getId()));
        }
        return response;
    }

    @GetMapping("/api/category/{id}")
    public List<ProjectEntryDTO> showProjects(@PathVariable long id){
        LinkedList<ProjectEntryDTO> projectList = new LinkedList<>();

        for (ProjectEntry element: this.projectEntryRepository.findAllByCategoryEntry_Id(id)) {
            projectList.add(new ProjectEntryDTO(
                    element.getId(),
                    element.gettitle(),
                    element.getText(),
                    element.getUrl(),
                    element.getDescription(),
                    element.getTechnology(),
                    element.getTime()
            ));
        }
        return projectList;
    }

    @PostMapping("/api/newcategory")
    public CategoryEntryDTO writeCategory(@RequestBody CategoryEntryDTO categoryEntryDTO) {
        categoryEntryRepository.save(new CategoryEntry(categoryEntryDTO.getTitle(), categoryEntryDTO.getText()));
        showCategories();
        return categoryEntryDTO;
    }


    @PostConstruct
    public void dummyData() {
        if (categoryEntryRepository.count() == 0) {
            categoryEntryRepository.save(new CategoryEntry("Game-App", "Hier findet ihr die Spiele der Consultants."));
            categoryEntryRepository.save(new CategoryEntry("Web-App", "Hier geht es zu den Blogs der Consultants."));
            categoryEntryRepository.save(new CategoryEntry("Abschlussprojekt", "Hier, der krönende Abschluss."));
        }
    }
}
