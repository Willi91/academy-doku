package de.awacademy.abschlussprojekt.dokubackend.project;

public class ProjectEntryDTO {

    private long id;
    private String title;
    private String text;
    private String url;
    private String description;
    private String technology;
    private int time;

    public ProjectEntryDTO() {
    }

    public ProjectEntryDTO(String title, String text) {
        this.title = title;
        this.text = text;
    }

    public ProjectEntryDTO(long id, String title, String text, String url, String description, String technology, int time) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.url = url;
        this.description = description;
        this.technology = technology;
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }
}
