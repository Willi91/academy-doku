package de.awacademy.abschlussprojekt.dokubackend.project;


import de.awacademy.abschlussprojekt.dokubackend.category.CategoryEntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.LinkedList;
import java.util.List;

@RestController
public class ProjectController {

    @Autowired
    private ProjectEntryRepository projectEntryRepository;
    @Autowired
    private CategoryEntryRepository categoryEntryRepository;



    @GetMapping("/api/details/{id}")
    public ProjectEntryDTO showProject(@PathVariable long id) {
        ProjectEntry projectEntry = projectEntryRepository.findById(id).orElseThrow();
        return new ProjectEntryDTO(projectEntry.getId(), projectEntry.gettitle(), projectEntry.getText(), projectEntry.getUrl(), projectEntry.getDescription(), projectEntry.getTechnology(), projectEntry.getTime());
    }

    @GetMapping("/api/allprojects")
    public List<ProjectEntryDTO> showProjects() {
        List<ProjectEntryDTO> allProjects = new LinkedList<>();
        for (ProjectEntry projectEntry : projectEntryRepository.findAllByOrderByCreatedAtDesc()) {
           allProjects.add(new ProjectEntryDTO(projectEntry.getId(), projectEntry.gettitle(), projectEntry.getText(), projectEntry.getUrl(), projectEntry.getDescription(), projectEntry.getTechnology(), projectEntry.getTime()));
        }
        return allProjects;
    }


    @GetMapping("/api/projects/{technology}")
    public List<ProjectEntryDTO> showProjectsFilter(@PathVariable String technology) {
        List<ProjectEntryDTO> allProjects = new LinkedList<>();
        for (ProjectEntry projectEntry : projectEntryRepository.findAllByTechnologyContains(technology.toLowerCase())) {
            allProjects.add(new ProjectEntryDTO(projectEntry.getId(), projectEntry.gettitle(), projectEntry.getText(), projectEntry.getUrl(), projectEntry.getDescription(), projectEntry.getTechnology(), projectEntry.getTime()));
        }
        return allProjects;
    }


    @PostMapping("/api/category/{id}")
    public ProjectEntryDTO writeProject(@RequestBody ProjectEntryDTO projectEntryDTO, @PathVariable long id) {
        projectEntryRepository.save(new ProjectEntry(projectEntryDTO.getTitle(),
                projectEntryDTO.getText(), projectEntryDTO.getUrl(), categoryEntryRepository.findById(id).orElseThrow(), projectEntryDTO.getDescription(), projectEntryDTO.getTechnology(), projectEntryDTO.getTime()));

        return showProject(id);
    }



    @DeleteMapping("/api/category/{id}")
    public void delete(@PathVariable long id) {
        projectEntryRepository.deleteById(id);
    }


    @PutMapping("/api/category/{id}")
    public ProjectEntryDTO update(@RequestBody ProjectEntryDTO projectEntryDTO, @PathVariable long id) {
        ProjectEntry projectEntry = projectEntryRepository.findById(id).orElseThrow();

        projectEntry.setTitle(projectEntryDTO.getTitle());
        projectEntry.setText(projectEntryDTO.getText());
        projectEntry.setUrl(projectEntryDTO.getUrl());
        projectEntry.setDescription(projectEntryDTO.getDescription());
        projectEntry.setTechnology(projectEntryDTO.getTechnology());
        projectEntry.setTime(projectEntryDTO.getTime());
        projectEntryRepository.save(projectEntry);

        return new ProjectEntryDTO(
                projectEntry.getId(), projectEntry.gettitle(), projectEntry.getText(), projectEntry.getUrl(), projectEntry.getDescription(), projectEntry.getTechnology(), projectEntry.getTime()
        );
    }


    @PostConstruct
    public void dummyData() {
        if (projectEntryRepository.count() == 0) {
            projectEntryRepository.save(new ProjectEntry("PongApplication", "Spiel", "https://gitlab.com/academy-doku/pong",categoryEntryRepository.getOne(1L),"Java Mini-Projekt", "Java JavaFx", 3) );
            projectEntryRepository.save(new ProjectEntry("Traffic Racer", "Spiel", "https://gitlab.com/academy-doku/traffic-racer", categoryEntryRepository.getOne(1L),"Java Mini-Projekt", "Java JavaFx", 3));
            projectEntryRepository.save(new ProjectEntry("SpaceCat", "Spiel", "https://gitlab.com/academy-doku/spacecat", categoryEntryRepository.getOne(1L),"Java Mini-Projekt", "Java JavaFx", 3));
            projectEntryRepository.save(new ProjectEntry("BlogKD", "Katis´ und Daniel´s Blog", "https://gitlab.com/academy-doku/blog", categoryEntryRepository.getOne(2L),"Java+Web Mini-Projekt in 2er Teams", "Java Spring Thymeleaf SQL Html CSS Bulma", 5));
            projectEntryRepository.save(new ProjectEntry("Blog", "Willi´s Blog", "https://gitlab.com/academy-doku/blog2", categoryEntryRepository.getOne(2L),"Java+Web Mini-Projekt in 2er Teams", "Java Spring Thymeleaf SQL Hibernate Html CSS Bulma", 5));
            projectEntryRepository.save(new ProjectEntry("Academy Doku", "Abschlussprojekt", "https://gitlab.com/academy-doku/academy-doku", categoryEntryRepository.getOne(3L),"Dokumentation und Verwaltung der Academy Projekte", "Java Spring SQL Html CSS Angular TypeScript", 10));

        }
    }

}
