import {Component, OnInit} from '@angular/core';
import {Project} from '../project';
import {ActivatedRoute, Router} from '@angular/router';
import {SecurityService} from '../security-service';
import {HttpClient} from '@angular/common/http';
import {Location} from '@angular/common';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  id: number;
  projectEntry: Project;

  constructor(private route: ActivatedRoute, private securityService: SecurityService,
              private httpClient: HttpClient, private location: Location, private router: Router) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params.id;
      this.httpClient.get<Project>('/api/details/' + this.id).subscribe(
        e => this.projectEntry = e
      );
    });
  }


  updateEntry(project: Project) {
    this.httpClient.put<Project>('/api/category/' + project.id, this.projectEntry)
      .subscribe(response => this.projectEntry = response);
  }

  goBack(): void {
    this.location.back();
  }

}
