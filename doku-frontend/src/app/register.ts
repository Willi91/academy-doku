export interface Register {
  id: string;
  username: string;
  password: string;
}
