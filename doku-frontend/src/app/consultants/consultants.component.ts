import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SecurityService} from '../security-service';
import {User} from '../user';

@Component({
  selector: 'app-consultants',
  templateUrl: './consultants.component.html',
  styleUrls: ['./consultants.component.css']
})
export class ConsultantsComponent implements OnInit {

  sessionUser: User | null = null;

  constructor(private httpClient: HttpClient, private securityService: SecurityService) {
  }

  ngOnInit() {
    this.securityService.getSessionUser()
      .subscribe(u => this.sessionUser = u);
  }

}
