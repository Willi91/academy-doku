import {Component, OnInit} from '@angular/core';
import {Category} from './category';
import {HttpClient} from '@angular/common/http';
import {User} from '../user';
import {SecurityService} from '../security-service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  id: number;
  categories: Category[];
  sessionUser: User | null = null;


  constructor(private httpClient: HttpClient, private securityService: SecurityService) {
  }

  ngOnInit() {
    this.httpClient.get<Category[]>('/api/overview')
      .subscribe(entries => {
        this.categories = entries;
      });
    this.securityService.getSessionUser()
      .subscribe(u => this.sessionUser = u);
  }
}
