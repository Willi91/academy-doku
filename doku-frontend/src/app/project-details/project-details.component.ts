import {Component, OnInit} from '@angular/core';
import {User} from '../user';
import {HttpClient} from '@angular/common/http';
import {SecurityService} from '../security-service';
import {ActivatedRoute} from '@angular/router';
import {Project} from '../project';


@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css']
})
export class ProjectDetailsComponent implements OnInit {

  id: number;
  projectEntry: Project;
  sessionUser: User | null = null;


  constructor(private route: ActivatedRoute, private securityService: SecurityService,
              private httpClient: HttpClient) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params.id;
      this.httpClient.get<Project>('/api/details/' + this.id).subscribe(
        e => this.projectEntry = e
      );
    });
  }

}
