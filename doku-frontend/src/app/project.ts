export interface Project {
  id: number;
  title: string;
  text: string;
  url: string;
  description: string;
  technology: string;
  categoryId: number;
  time: number;
}
