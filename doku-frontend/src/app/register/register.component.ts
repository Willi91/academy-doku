import {Component, OnInit} from '@angular/core';
import {User} from '../user';
import {SecurityService} from '../security-service';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  sessionUser: User | null = null;
  registryForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private securityService: SecurityService,
  ) { }

  ngOnInit() {
    this.registryForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  get f() { return this.registryForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registryForm.invalid) {
      return;
    }

    this.loading = true;
    this.securityService.register(this.registryForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(['/login']);
        },
        error => {
          this.loading = false;
        });
  }

  /*registerUser(userRegistrationDTO: UserRegistrationDTO){
  this.registrationService.registerUser(userRegistrationDTO)
    .subscribe(r => console.log(r));
  }

  get username() {
    return this.registryForm.get('username');
  }

  get password() {
    return this.registryForm.get('password');
  }

  get password2() {
    return this.registryForm.get('password2');
  }
 */

}
