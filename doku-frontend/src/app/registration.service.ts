import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from './user';
import {Observable} from 'rxjs';
import {UserRegistrationDTO} from './UserRegistrationDTO';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  constructor(private httpclient: HttpClient) { }

  registerUser(userRegistrationDTO: UserRegistrationDTO): Observable<number> {
  return this.httpclient.post<number>('/api/register', userRegistrationDTO);
  }
}
