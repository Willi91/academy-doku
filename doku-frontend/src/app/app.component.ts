import {Component, OnInit} from '@angular/core';
import {SecurityService} from './security-service';
import {User} from './user';
import {Router} from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  sessionUser: User|null = null;

  constructor(private securityService: SecurityService, private router: Router) { }
  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
  }

  logout() {
    this.securityService.logout();
    this.router.navigate(['']);
  }
}
