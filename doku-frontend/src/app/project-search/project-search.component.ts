import {Component, OnInit} from '@angular/core';
import {Project} from '../project';
import {HttpClient} from '@angular/common/http';
import {FormControl} from '@angular/forms';
import {User} from '../user';
import {SecurityService} from '../security-service';


@Component({
  selector: 'app-project-search',
  templateUrl: './project-search.component.html',
  styleUrls: ['./project-search.component.css']
})
export class ProjectSearchComponent implements OnInit {
  myControl = new FormControl();
  allProjects: Project[] = [];
  filteredProjects: Project[] = [];
  term = '';
  technology = '';
  sessionUser: User | null = null;

  constructor(private securityService: SecurityService, private http: HttpClient) {
  }

  search(): void {
    this.filteredProjects = this.allProjects.filter(project => project.title.toLowerCase().includes(this.term.toLowerCase()));
  }

  ngOnInit(): void {
    this.http.get<Project[]>('/api/allprojects')
      .subscribe(entries => {
        this.allProjects = entries;
      });
    this.securityService.getSessionUser()
      .subscribe(u => this.sessionUser = u);
  }

}
